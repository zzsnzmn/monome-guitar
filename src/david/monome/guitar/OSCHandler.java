package david.monome.guitar;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiUnavailableException;

import com.illposed.osc.OSCListener;
import com.illposed.osc.OSCMessage;
import com.illposed.osc.OSCPortIn;

public class OSCHandler {
	
	public OSCHandler() throws MidiUnavailableException{
		final Monome monome = new Monome();
		final MidiHandler midiOut = new MidiHandler();
		final OSCSender sender = new OSCSender();
		
		try{
			OSCPortIn receiver = new OSCPortIn(8000);
			OSCListener listener = new OSCListener() {
				public void acceptMessage(java.util.Date time, OSCMessage message){
					//System.out.println("Message received!"); //stdout test
					Integer[] packet = new Integer[3];
					packet[0] = (Integer) message.getArguments()[0];
					packet[1] = (Integer) message.getArguments()[1];
					packet[2] = (Integer) message.getArguments()[2];
					if((packet[2]).equals(1)){
						if((packet[0].equals(1)) && (packet[1].equals(0))){
							monome.setChromatic();
							monome.setChannel(0);
							sender.sendLed(0, 0, 0);
							sender.sendLed(1, 0, 1);
						}
						else if((packet[0].equals(0)) && (packet[1].equals(0))){
							monome.setGuitar();
							monome.setChannel(1);
							sender.sendLed(0, 0, 1);
							sender.sendLed(1, 0, 0);
						
						System.out.println("Press on: " + packet[0] + 
							" " + packet[1]);
					}
						if(packet[1] > 0 && packet[2].equals(1)){	
							try {
								midiOut.noteOn(monome.getNote(message.getArguments()[0], message.getArguments()[1]), 0);
								sender.sendLed((Integer) message.getArguments()[0],(Integer) message.getArguments()[1], 1);
							} catch (InvalidMidiDataException e) {
								e.printStackTrace();
							} catch (MidiUnavailableException e) {
								e.printStackTrace();
							}
						}
					}
					else if(((message.getArguments()[2]).equals(0)) && ((Integer) message.getArguments()[1]) > 0){
						System.out.println("Press off: " + message.getArguments()[0] + " " + 
								message.getArguments()[1]);
						try {
							midiOut.noteOff(monome.getNote(message.getArguments()[0], message.getArguments()[1]), 0);
							sender.sendLed(packet[0],packet[1], 0);
						} catch (InvalidMidiDataException e) {
							e.printStackTrace();
						} catch (MidiUnavailableException e) {
							e.printStackTrace();
						}
					}
				}
			};
			receiver.addListener("/monome/press", listener);
			receiver.startListening();
			}
			
			catch (Exception e){
				System.out.println(e.toString());	
			}
	}

}
