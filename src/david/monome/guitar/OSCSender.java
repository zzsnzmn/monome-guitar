package david.monome.guitar;

import java.net.InetAddress;

import com.illposed.osc.OSCMessage;
import com.illposed.osc.OSCPortOut;

public class OSCSender {
	
	public OSCSender(){
	}
	
	public void sendLed(int a, int b, int c){ //method for sending LED on messages
		try { 
			
			InetAddress ina = InetAddress.getByName("127.0.0.1"); //OSC messages sent on localhost
	        OSCPortOut sender = new OSCPortOut(ina, 8080);		  //create OSC port out to send to the monome
			
            Object argsout[] = new Object[3]; //packet container
            argsout[0] = new Integer(a);
            argsout[1] = new Integer(b);
            argsout[2] = new Integer (c);

            OSCMessage msg = new OSCMessage("/monome/led", argsout); //create the message with the array

            sender.send(msg);
		}
    
		catch (Exception e) {
			System.out.println(e.toString());
		}
	}
	
	public void clearLed(){ //method for clearing the LEDs
		try {
            InetAddress ina = InetAddress.getByName("127.0.0.1");
            OSCPortOut sender = new OSCPortOut(ina, 8080);
            OSCMessage msg = new OSCMessage("/monome/led clear");

            sender.send(msg);
		}
		
		catch (Exception e){
			System.out.println(e.toString());
		}
	}

}

