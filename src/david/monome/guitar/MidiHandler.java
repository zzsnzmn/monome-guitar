package david.monome.guitar;

import javax.sound.midi.*;

public class MidiHandler {

	public MidiHandler() throws MidiUnavailableException{
	
		MidiDevice.Info[] infos = MidiSystem.getMidiDeviceInfo();  //creates an array of all midi devices
		for (int i = 0; i < infos.length; i++){					   //loops through a list of all midi devices
			try{												   //and prints each device.
				MidiDevice device = MidiSystem.getMidiDevice(infos[i]);
				System.out.println(infos[i]);
				device.close();									   //closes out all devices
			} catch (MidiUnavailableException e){
				//handle exceptions	--- not super graceful :\
			}
		}	
	}
		
	
	public void noteOn(int noteNumber, int channel) throws InvalidMidiDataException, MidiUnavailableException{
		  ShortMessage myMsg = new ShortMessage();
		  // Start playing the note noteNumber, 
		  // somewhat loud (velocity = 93).
		  myMsg.setMessage(ShortMessage.NOTE_ON, channel, noteNumber, 93);
		  long timeStamp = -1; //placeholder for timestamp (not *really* needed)
		  Receiver	 rcvr = MidiSystem.getReceiver();
		  rcvr.send(myMsg, timeStamp);
		
	}
	
	public void noteOff(int noteNumber, int channel) throws InvalidMidiDataException, MidiUnavailableException{
		  ShortMessage myMsg = new ShortMessage();
		  // Start playing the note noteNumber, 
		  // moderately loud (velocity = 93).
		  myMsg.setMessage(ShortMessage.NOTE_OFF, channel, noteNumber, 93);
		  long timeStamp = -1; //placeholder/default
		  Receiver	 rcvr = MidiSystem.getReceiver();   //retrieve a midi output
		  rcvr.send(myMsg, timeStamp);					//send to that midi output
		
	}

	
	
}
