package david.monome.guitar;

/*
 * Class meant to represent a monome device
 */

public class Monome {
	
	private int baseNote;
	private String prefix;
	private int channel;
	private int[][] activeLeds = new int[8][8];
	private int[][] noteArray = new int[8][8];
	
	public Monome(){
		baseNote = 64;
		prefix = "/monome";
		setGuitar();
	}
	

	public Monome(int[][] noteArray){
		this.setNoteArray(noteArray);
	}

	public void setActiveLeds(int[][] activeLeds) {
		this.activeLeds = activeLeds;
	}

	public int[][] getActiveLeds() {
		return activeLeds;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setNoteArray(int[][] noteArray) {
		this.noteArray = noteArray;
	}

	public int[][] getNoteArray() {
		return noteArray;
	}
	
	public void setPoint(int x, int y, int state){
		this.activeLeds[x][y] = state;
	}
	
	public int getPoint(int x, int y){
		return this.activeLeds[x][y];
	}
	
	public int getNote(Object x, Object y){
		return this.noteArray[(Integer) x][(Integer) y];
	}
	
	public void setChromatic(){
		baseNote = 36;
		for(int i = 7; i !=0 ; i--){
			for(int j = 0; j < 8; j++){
				activeLeds[j][i] = 0;		//sets the state of all leds to off (default)
				noteArray[j][i] = baseNote;	//sets the note # of all buttons
				baseNote++;
			}
		}
	}
	
	public void setGuitar(){
		int z = 0;
		int[] guitarSteps = {52, 57, 62, 67, 71, 76, 81, 85}; //should be a for each loop or dynamic allocation
		for(int i = 7; i !=0; i--){
			for(int j = 0; j < 8; j++){
				noteArray[j][i] = guitarSteps[z]+j;
			}
			z++;
		}
	}
	
	public void setChannel(int channel){
		this.channel = channel;
	}
	
	public int getChannel(){
		return channel;
	}
	
}
