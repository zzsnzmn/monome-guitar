package david.monome.guitar;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiUnavailableException;

import david.monome.guitar.MidiHandler;;


/*
 * Basic test to ensure that MIDI is working. Hooked up to the 
 * default Java Sound Synthesizer.
 */

public class MidiTest {
	
//	static MidiHandler midiSender;
	
	public static void main(String[] args) throws 
	MidiUnavailableException, InvalidMidiDataException, InterruptedException{
//		System.out.println("hello midi");
//		Monome monome = new Monome();
//		System.out.println(monome.getPrefix());
//		for(int i = 0; i < 8; i++){
//			for(int j = 0; j < 8; j++){
//				System.out.println(monome.getNote(i, j));
//			}
//		}

//		MidiDevice device;
//		MidiDevice.Info[] infos = MidiSystem.getMidiDeviceInfo();
//		device = MidiSystem.getMidiDevice(infos[0]);
//		for (int i = 0; i < infos.length; i++){
//			try{
//				device = MidiSystem.getMidiDevice(infos[i]);
//				System.out.println(infos[i]);
//				device = MidiSystem.getMidiDevice(infos[i]);
//			} catch (MidiUnavailableException e){
//				
//			}
//			if (device instanceof Synthesizer){
//				System.out.println("There is a Synthesizer:" + infos[i]);
//			}
//		}
//		  ShortMessage myMsg = new ShortMessage();
//		  myMsg.setMessage(ShortMessage.NOTE_ON, 0, 60, 93);
//		  long timeStamp = -1;
//		  Receiver	 rcvr = MidiSystem.getReceiver();
//		  rcvr.send(myMsg, timeStamp);
//		

		MidiHandler midiOut = new MidiHandler();
		for(int i = 0; i < 20; i=i+2){
			midiOut.noteOn(50+i, 1);	
			Thread.sleep(500);
			midiOut.noteOff(50+i, 1);
		}
	}

}
