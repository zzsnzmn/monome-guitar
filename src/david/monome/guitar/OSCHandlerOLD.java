package david.monome.guitar;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiUnavailableException;

import com.illposed.osc.OSCListener;
import com.illposed.osc.OSCMessage;
import com.illposed.osc.OSCPortIn;



public class OSCHandlerOLD {
	
	String prefix = "/monome";
	
	public OSCHandlerOLD() throws MidiUnavailableException{
		final Monome monome = new Monome();  //create monome object (needed for note references
		final MidiHandler midiOut = new MidiHandler(); //midi output
		final OSCSender sender = new OSCSender();	   //OSC output to the monome (should specify what port in the constructor...)
		
		try{
			OSCPortIn receiver = new OSCPortIn(8000);
			OSCListener listener = new OSCListener() {
				public void acceptMessage(java.util.Date time, OSCMessage message){
					//System.out.println("Message received!");
					Integer[] packet = new Integer[3]; //create an array of the OSC packet values
					packet[0] = (Integer) message.getArguments()[0];	//cast each to "Integer"
					packet[1] = (Integer) message.getArguments()[1];	//again
					packet[2] = (Integer) message.getArguments()[2];	//one more time
					if((message.getArguments()[2]).equals(1)){ //if button is pressed
						if((message.getArguments()[0].equals(1)) && (message.getArguments()[1].equals(0))){ //if in 0, 1 (cartesian)
							monome.setChromatic(); //change noteArray from guitar tuning to chromatic
							monome.setChannel(0);  //set midi channel to 1
							sender.sendLed(0, 0, 0); //refresh leds
							sender.sendLed(1, 0, 1); //refresh leds
						}
						else if((message.getArguments()[0].equals(0)) && (message.getArguments()[1].equals(0))){ //if in 0, 0
							monome.setGuitar();	//change noteArray to guitar tuning
							monome.setChannel(1); //set channel to 1
							sender.sendLed(0, 0, 1); //refresh LEDs
							sender.sendLed(1, 0, 0); //refresh LEDS
						
						System.out.println("Press on: " + message.getArguments()[0] + 
							" " + message.getArguments()[1]); //print arguments (for debugging...)
					}
						if((Integer) message.getArguments()[1] > 0 && message.getArguments()[2].equals(1)){	
							try {
								midiOut.noteOn(monome.getNote(message.getArguments()[0], message.getArguments()[1]), 0);
								sender.sendLed((Integer) message.getArguments()[0],(Integer) message.getArguments()[1], 1);
							} catch (InvalidMidiDataException e) {
								e.printStackTrace();
							} catch (MidiUnavailableException e) {
								e.printStackTrace();
							}
						}
					}
					else if(((message.getArguments()[2]).equals(0)) && ((Integer) message.getArguments()[1]) > 0){
						System.out.println("Press off: " + message.getArguments()[0] + " " + 
								message.getArguments()[1]);
						try {
							midiOut.noteOff(monome.getNote(message.getArguments()[0], message.getArguments()[1]), 0);
							sender.sendLed((Integer) message.getArguments()[0],(Integer) message.getArguments()[1], 0);
						} catch (InvalidMidiDataException e) {
							e.printStackTrace();
						} catch (MidiUnavailableException e) {
							e.printStackTrace();
						}
					}
				}
			};
			receiver.addListener((getPrefix() + "/press"), listener);   //listen for incoming /press messages
			receiver.startListening();									//start the listener process
			}
			
			catch (Exception e){
				System.out.println(e.toString());	
			}
	}
	
	public void setPrefix(String p){
		prefix = p;
	}
	
	public String getPrefix(){
		return prefix;
	}
	
	
}
